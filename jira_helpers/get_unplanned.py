from datetime import datetime

from jira_helpers.jira_auth import auth

jiraClient = auth()

sprint_id = '338'
sprint_start_date_iso = ''
sprint_issues = ''


def get_current_sprint_issues():
    global sprint_issues
    jql = 'filter=20621'
    sprint_issues = jiraClient.search_issues(jql, expand='changelog')
    return sprint_issues


def find_issues_added_after_sprint(issues=None):
    sprint_date = datetime.strptime(sprint_start_date_iso, "%Y-%m-%dT%H:%M:%S%z")
    issues_added_after_sprint = set()
    if issues is None:
        issues = sprint_issues
    for issue in issues:
        for history in issue.changelog.histories:
            for item in history.items:
                if item.field == "Sprint" and item.to == sprint_id:
                    entry_date = datetime.strptime(history.created, "%Y-%m-%dT%H:%M:%S.%f%z")
                    if entry_date > sprint_date:
                        issues_added_after_sprint.add(issue.key)
    return issues_added_after_sprint


def set_sprint_data(id=None):
    global sprint_start_date_iso
    sprint = jiraClient.sprint(id)
    sprint_start_date_iso = sprint.isoStartDate


if __name__ == "__main__":
    get_current_sprint_issues()
    set_sprint_data()
    find_issues_added_after_sprint()
