import os

from jira import JIRA

def auth():
    username = os.environ['JIRA_USER']
    key = os.environ['JIRA_KEY']
    jira = JIRA(
        server='https://totalexpert.atlassian.net',
        basic_auth=(username, key)
    )

    return jira