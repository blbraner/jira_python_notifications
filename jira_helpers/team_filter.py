import os

import pytz
from jira import JIRA
import pync

def auth():
    username = os.environ['JIRA_USER']
    key = os.environ['JIRA_KEY']
    jira = JIRA(
        server='https://totalexpert.atlassian.net',
        basic_auth=(username, key)
    )

    return jira


def get_team_filter():
    jira = auth()
    jql = 'filter=20409'
    issues = jira.search_issues(jql)
    num_of_issues = len(issues)
    print(num_of_issues)

get_team_filter()