from datetime import datetime

import pytz
import pync

from src.jira_auth import auth

def get_issues_needing_severity():
    jira = auth()
    jql = 'filter=20477'
    issues = jira.search_issues(jql)
    num_of_issues = len(issues)

    
    if(num_of_issues > 0):
        message = f"Defects Needing Severity {num_of_issues} \n"
        message = issue_age_message(issues, message)

        pync.notify(message, title="Defects Needing Severity", open="https://totalexpert.atlassian.net/issues/?filter=20477")


def issue_age_message(issues, message):

    current_time = datetime.now()
    timezone = pytz.timezone("America/Chicago")
    current_time_aware = timezone.localize(current_time)
    for issue in issues:
        created_time = datetime.strptime(issue.fields.created,"%Y-%m-%dT%H:%M:%S.%f%z")
        age = current_time_aware - created_time
        if age.days >= 2:
            message = message + f"{issue.key} is approching SLA \n"
    return message


get_issues_needing_severity()
